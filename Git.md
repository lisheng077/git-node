## 1.Git介绍



## 1.CSDN关于Git详解地址

https://blog.csdn.net/u011535541/article/details/83379151?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522166311652916781432990754%2522%252C%2522scm%2522%253A%252220140713.130102334..%2522%257D&request_id=166311652916781432990754&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~top_positive~default-1-83379151-null-null.142^v47^pc_rank_34_default_2,201^v3^control&utm_term=git%E6%95%99%E7%A8%8B&spm=1018.2226.3001.4187





免费，开源的**分布式版本控制系统**（工具），可以快速高效的从小型到大型的各种项目

Git易于学习，占地面积小，性能极快。它具有廉价的本地库，方便的暂存区域和多个工作流分支等特性。

官网地址：https://git-scm.com/



## 2.何为版本控制



版本控制是一种记录文件内容变化，以便将来查阅特定版本修订情况的系统。

版本控制其实最重要的是可以记录修改历史记录，从而让用户能够查看历史版本，方便版本切换。（有点类似MVCC）。

**为什么需要版本控制？**

方便个人开发过渡到团队协作。



## 3.分布式解决了集中式版本控制的哪些问题



1.分布式解决了集中式在服务器·断网的情况下不能开发的问题，这是因为分布式版本控制是在本地进行的；

2.每个客户端保存的也都是整个完整的项目（包含项目修改的历史记录，更加安全）



## 4.Git的工作原理/流程

![image-20220914100145099](C:\Users\lenovo\AppData\Roaming\Typora\typora-user-images\image-20220914100145099.png)

**代码托管中心：是基于网络服务的远程代码仓库，一般我们简单称为远程库。**

**常有：局域网：GitLab；互联网：Github（外网），Gitee码云（国内网站）**



## 5.Git 命令-设置用户签名



![image-20220914143055046](C:\Users\lenovo\AppData\Roaming\Typora\typora-user-images\image-20220914143055046.png)

![image-20220914143029917](C:\Users\lenovo\AppData\Roaming\Typora\typora-user-images\image-20220914143029917.png)



**首次安装Git后，必须设置用户名和邮箱等，否则提交代码时会报错。**

**签名的作用是区分不同的操作者身份，用户的签名信息在每一个版本提交信息中能够看到，以此确认本次提交谁做的。**

**注意：这里设置用户签名和将来登录GitHub（或其他代码托管中心）的账号没有任何关系**



## 6.Git 命令-初始化本地库



找到项目所在的目录，右击点击Git bash here，执行命令 Git init

![image-20220914151433132](C:\Users\lenovo\AppData\Roaming\Typora\typora-user-images\image-20220914151433132.png)

 

## 7.Git 命令-查看本地库状态



![image-20220914153647209](C:\Users\lenovo\AppData\Roaming\Typora\typora-user-images\image-20220914153647209.png)

在工作区创建一个 hello.txt 的文件，进入页面：

![image-20220914153919069](C:\Users\lenovo\AppData\Roaming\Typora\typora-user-images\image-20220914153919069.png)

**按  insert  键，就可以输入内容**

![image-20220914154041383](C:\Users\lenovo\AppData\Roaming\Typora\typora-user-images\image-20220914154041383.png)

**按  esc  键 退出编辑状态**

**（退出编辑状态也可以使用 vim 中 的快捷键，像将光标放在某一行上，按 yy，表示复制；在按 p  ，表示粘贴）**

**按  shift+“；”，再输入 wq  回车。就可以保存。**



**输入  Git  status  查看本地库状态**

![image-20220914155125741](C:\Users\lenovo\AppData\Roaming\Typora\typora-user-images\image-20220914155125741.png)

1. on branch master ：代表所在分支
2.  no commits yet ：代表没有提交过东西
3. nothing to commit：代表 没有东西可以提交
4. untracked files ：代表未被追踪的文件。里面 demo1.txt等文件 是红色的 ，这就说明有东西存在，但是它只存在于 工作区         **（这里的追踪  指的就是   将它添加到  暂存区  ）**
5. nothing added to。。。。。 ：提示我们需要 使用 Git add 命令去追踪 它



**输入  ll  ，可以查看工作区中 有哪些 东西**

![image-20220914161500450](C:\Users\lenovo\AppData\Roaming\Typora\typora-user-images\image-20220914161500450.png)



## 8.Git 命令-添加暂存区



使用 Git add 添加暂存区，在使用 Git status 查看本地库状态，会发现已添加到  暂存区的 东西 颜色  变成 

绿色了。未添加的仍然是  红色的。

![image-20220914161817364](C:\Users\lenovo\AppData\Roaming\Typora\typora-user-images\image-20220914161817364.png)

Git rm --cached  文件名：命令删除 暂存区中 的文件；但是 工作区  里面的文件 依旧存在。

![image-20220914164628563](C:\Users\lenovo\AppData\Roaming\Typora\typora-user-images\image-20220914164628563.png)

rm 。。。。 ：表示删除成功



## 9.Git 命令-提交本地库

![](C:\Users\lenovo\AppData\Roaming\Typora\typora-user-images\image-20220914183752917.png

![image-20220914185411088](C:\Users\lenovo\AppData\Roaming\Typora\typora-user-images\image-20220914185411088.png) 

使用 **Git commit -m “版本名称或者是描述”  文件名**  ：

其中 红色方框里面代表  **一个文件 被修改，三行新增**

![image-20220914183717555](C:\Users\lenovo\AppData\Roaming\Typora\typora-user-images\image-20220914183717555.png)

**使用  Git reflog  命令查看  提交日志**

（重点理解一下）

**其中 b027722 是版本号（其实是真实版本号的前七位）；head。。 ：表示当前指针 指向 master 分支，还表示当前指针指向哪个版本。本质 是  指针指向master分支 ；master分支在指向 版本，在  版本 穿梭中，本质是 head 指向 master不会动 ，动的是  master 指向 版本的指针；**

**first。。。 ：表示 提交命令时写入的  描述或者是版本名**



**使用  Git log  查看完整详细的  日志**

**需要注意区别是  它显示 的是 完整的 版本号 ；同时也会显示 author（也就是我们设置的 用户签名 包括 用户名和邮箱）**。使我们看见 是谁提交了本地库

![image-20220914184546876](C:\Users\lenovo\AppData\Roaming\Typora\typora-user-images\image-20220914184546876.png)



## 10.Git 命令-修改文件



输入vim 文件名 ，进入编辑状态，修改数据

![image-20220914190830242](C:\Users\lenovo\AppData\Roaming\Typora\typora-user-images\image-20220914190830242.png)



完成修改后。查看本地库状态，出现提示  modified  表示该文件被修改，并且是红色的，表示该文件是在本地区，而不是在 暂存区，更没有提交被修改 后的版本文件

![image-20220914190949064](C:\Users\lenovo\AppData\Roaming\Typora\typora-user-images\image-20220914190949064.png)



**经过再一次的add 和 commit 之后，查看 reflog  会看到第二张图 的 改变，指针指向了 修改后的 版本文件** ，

在观察 第一张图  在提交之后 会提示  **一个文件 被修改；一行新增；一行删除**、

这是因为  把第二个版本提交时，实际上是把第一个版本 的 **被修改 的 行 先做删除 处理，如然后再将 修改 的 行 新增到 被删除 的行位置上**

![image-20220914191457254](C:\Users\lenovo\AppData\Roaming\Typora\typora-user-images\image-20220914191457254.png)

![image-20220914191654837](C:\Users\lenovo\AppData\Roaming\Typora\typora-user-images\image-20220914191654837.png)



## 11.Git 命令-版本穿梭



使用  Git reset -- hard 版本号，命令；

![image-20220914193834688](C:\Users\lenovo\AppData\Roaming\Typora\typora-user-images\image-20220914193834688.png)

可以看到  第二条框中表示 master 指针已经指向  第二个版本；第一个方框表示 记录了 一个日志，表示把一个指针或者版本 穿梭 到了 哪个版本。当然 我们也可以 向后版本进行穿梭。

![image-20220914194023831](C:\Users\lenovo\AppData\Roaming\Typora\typora-user-images\image-20220914194023831.png)

**使用  cat  文件名 ， 命令。可以看到文件的内容；这里的文件有两种情况（一种是 提交了的文件，那就显示提交了并且master指针指向的  版本文件；第二种是  未提交 和 提交过但是被修改的新版本未提交的文件，这些文件都在工作区，使用 cat 查看 也是 查看工作区的文件 内容）**

![image-20220914194420707](C:\Users\lenovo\AppData\Roaming\Typora\typora-user-images\image-20220914194420707.png)



## 12.Git 分支 理解与优点



理解：在版本控制过程中，同时推进多个任务，我们就可以为每个任务单独创建分支。使用分支意味着程序员可以吧自己的工作从开发主线上分离开来，开发自己的分支的时候，不会影响主分支的运行，对于初学者而言，分支可以理解为副本。一个分支就是一个单独的副本。**（其分支 底层是 指针 的使用）**

**我们可以想象 日常生活 当中 软件的更新，在更新开发过程中，我们不能影响原版本 的运行，当我们更新开发完成以后，再将 指针指向 新版本 来达到 更新的效果，在仔细想想，我们有时候 不更新 软件也能 用旧版本 的软件，这是为什么？，这就是因为  旧版本的 分支依旧存在，并没有被 改变，软件依旧能运行。**

**分支的好处：同时并行推进多个功能的开发，提高开发效率；**

**各个分支在开发过程中，如果某个分支开发失败，不会对其他的分支有任何的影响。失败的分支删除重新开始即可。**



## 13.Git 分支-查看&创建&切换



**如下图： 使用 Git branch 分支名，命令创建一个分支**

​				**使用 Git branch -v，命令查看 有分支，其中 有 * 号并且 为绿色 的表示 当前所在 分支**

​				**使用 Git checkedout  分支名 ，命令 切换到指定的 分支上 ，会发现 后面 的分支已经更改成功**

![image-20220914201923304](C:\Users\lenovo\AppData\Roaming\Typora\typora-user-images\image-20220914201923304.png)



## 14.Git 分支-合并分支（正常合并）

在 hot-fix 分支上做修改，并提交。再切换到  master 

![image-20220914203555188](C:\Users\lenovo\AppData\Roaming\Typora\typora-user-images\image-20220914203555188.png)



**使用  Git merge 分支名，命令将 hot-fix 分支 合并到  master 中**，

查看 合并后的 文件内容，发现没有报错，并且内容是之前 hot-fix中修改的 文件内容；这是因为 转换到 master后，它并没有对 文件做修改  就  将 hot-fix 合并，这就是正常合并。

![image-20220914203715409](C:\Users\lenovo\AppData\Roaming\Typora\typora-user-images\image-20220914203715409.png)



## 15.Git 分支-合并分支（冲突合并）



冲突产生的原因：

合并分支时，两个分支在同一文件的同一个位置有两套完全不同的修改，Git无法替我们决定使用哪一个，必须认为决定新代码内容

![image-20220914210011813](C:\Users\lenovo\AppData\Roaming\Typora\typora-user-images\image-20220914210011813.png)

查看状态（检测到有文件有俩处修改）

both modified 

![image-20220914210511777](C:\Users\lenovo\AppData\Roaming\Typora\typora-user-images\image-20220914210511777.png)

只能人为决定新代码

![image-20220914210816042](C:\Users\lenovo\AppData\Roaming\Typora\typora-user-images\image-20220914210816042.png)

人为修改新代码之后为：（直接在上面图中 按 insert 键，做相关修改）

![image-20220914211021396](C:\Users\lenovo\AppData\Roaming\Typora\typora-user-images\image-20220914211021396.png)



为什么还是  master|merging

因为还没有提交，注意提交过程中 commit 后面 不能 写 文件名了，否则会报错

![image-20220914211609448](C:\Users\lenovo\AppData\Roaming\Typora\typora-user-images\image-20220914211609448.png)



**注意：创建分支的本质就是多创建一个指针，由head拿到这个指针去指向master或者是其他的分支名，表示我们当前所处在哪个分支上面。**



## 16.Git 团队协作



**Git的版本控制都是在本地库中，所以我们称Git为分布式版本控制工具**

1.团队内协作：

​					**push：**把本地库中的代码推送到代码托管中心（GitHub，Gitee码云）（要求是同一个团队的，才能够直接push到团队的代码托管中心），**（代码托管中心又叫远程库）**

​					**clone：**将代码托管中心的代码克隆（完整的复制下来）到自己的本地库中，

​					**pull：**拉取远程库中的代码，用来**更新**自己本地库中代码，

2.跨团队协作：

​					**fork：**将对方团队的远程库作为**一个分支**，完整的复制一份到自己的远程库中

​					**clone，push**，

​					**pull request：**拉取请求，告诉对方我已经协作完成，希望对方获取

​					经过团队对协作完成的代码审核通过后，就可以    **↓**

​					**merge：**将对方的代码与自己的代码合并

​					**pull。**



## 17.Git-GitHub-创建远程库&创建别名



**在GitHub用户页面右上角点击+号，选择newrepository，写入远程仓库名，一般写的名字与本地库一样的。**

**开源思想，建议选择public**，最后选择createrepository

![image-20220915105454358](C:\Users\lenovo\AppData\Roaming\Typora\typora-user-images\image-20220915105454358.png)



**第一个框中表示有两个不同协议的链接，图中显示的是https协议的远程库链接；**

![image-20220915105326339](C:\Users\lenovo\AppData\Roaming\Typora\typora-user-images\image-20220915105326339.png)



**为什么要创建别名，考虑链接名字太长，记不住甚至记错，可以为链接取一个别名，方便本地库对远程库的**

**push（推送），pull（拉取），（直接使用别名即可操作）**

**使用 Git remote -v  查看别名**

**使用 Git remote add （取别名）（远程库中的地址），命令创建别名**

**再次查看会发现 有两行别名，观察发现两行别名一样，但是后缀不一样，我们可以理解为 push（推送）可以用别名，fetch（拉取，克隆）也可以用别名。**



**注意：**

**这个别名 在该项目或者文件（指图中 d/git-demo）下的其他分支也是存在可用的**



![image-20220915105205544](C:\Users\lenovo\AppData\Roaming\Typora\typora-user-images\image-20220915105205544.png)



## 18.Git-GitHub-推送本地库到远程库



使用 Git push （别名） （分支名），命令将 分支内已提交至本地库的 代码或者文件  推送到 GitHub 上

![image-20220915142802286](C:\Users\lenovo\AppData\Roaming\Typora\typora-user-images\image-20220915142802286.png)



## 19.Git-GitHub-拉取远程库到本地库



**使用  Git pull （别名）（分支名），命令  从GitHub 上拉取更改后的文件到  本地库中，、**

**注意： 从 远程库 中拉取到 本地库 中 的文件 是 已经自动提交到 本地库了的**

![image-20220915144730180](C:\Users\lenovo\AppData\Roaming\Typora\typora-user-images\image-20220915144730180.png)



## 20.Git-GitHub-克隆远程库到本地



**使用  Git clone  （远程库的 地址），命令**

![image-20220915150548248](C:\Users\lenovo\AppData\Roaming\Typora\typora-user-images\image-20220915150548248.png)



**查看文件 ，发现 它不但 把文件 克隆下来，还把远程库 的   .git  配置文件克隆下来了；**

**克隆的本质是做了三个步骤： 1.拉取代码  2. 初始化本地仓库  3. 创建别名**

![image-20220915150418651](C:\Users\lenovo\AppData\Roaming\Typora\typora-user-images\image-20220915150418651.png)



**进入到  git-demo  中，查看 别名  ，发现  自动  创建了  别名  orgin（记住）**

![image-20220915151031241](C:\Users\lenovo\AppData\Roaming\Typora\typora-user-images\image-20220915151031241.png)





#### 20.1   小总结

​			 **拉取是在原有基础上 合并当前代码，克隆是克隆到文件夹，前者需要登录**

## 21.Git-GitHub-团队内协作



**团队中的人 邀请你 成为 团队的一员后，你接受后就可以在自己的 GitHub 上看见 团队项目**，然后就可以

Git push  （远程库地址或者别名）（分支名），命令 推送 本地库到远程库  ；  也可以 通过  

git pull （别名）（分支名） ，命令拉取 远程库到本地库协作

​	

## 22.Git-idea集成Git-环境准备



创建 git.ignore  文件，并在git.config 文件 中引用

![image-20220915192958376](C:\Users\lenovo\AppData\Roaming\Typora\typora-user-images\image-20220915192958376.png)



再到 idea 中 配置 Git使用环境，找到安装 的Git .exe 文件位置，点击 test  显示 Git版本号就说明 成功。



![image-20220915193343999](C:\Users\lenovo\AppData\Roaming\Typora\typora-user-images\image-20220915193343999.png)



## 21.Git-idea集成Git-初始化&添加&提交



创建新的maven 项目 ，点击根目录 发现 并 没有 git 选项，说明 我们还没有将项目交给 Git 管理，我们需要 先初始化  Git；点击  VCS  （version  controller  setting），选择   创建 Git本地库，默认选择的是 项目的根目录即可。

![image-20220916100025148](C:\Users\lenovo\AppData\Roaming\Typora\typora-user-images\image-20220916100025148.png)

![image-20220916100345258](C:\Users\lenovo\AppData\Roaming\Typora\typora-user-images\image-20220916100345258.png)

**右击选择的文件  会发现  有 Git 选项 ，可以发现  里面  有 add  和 commit 操作选项；**

观察发现  文件 为 **红色时** 表示 文件未被追踪，只在工作区，点击  **add  编变成绿色**，表示  被追踪在暂存区， 再点击 commit 变成 **正常的黑色** ，表示 被提交 到 本地库，为 方便操作 ，我们可以**直接在根目录下进行add和commit**



## 22.Git-idea集成Git-切换版本



在已提交的 代码 上 做修改 ，发现 文件从正常的黑色 变成  **蓝色，**表示 **该文件被提交过，但是被修改了，需要从新提交到 本地库中**；

在根目录 git.text 下重新提交后 变回 黑色。

![image-20220915201237974](C:\Users\lenovo\AppData\Roaming\Typora\typora-user-images\image-20220915201237974.png)



**切换版本：点击下方的Git  在选择 log ；可以看到 已提交 的 版本，最右侧 可以看到 相应 版本 提交 的 信息**

**黄色  head  指的 是 指向 哪个 分支；**

**紫色 master（分支名），表示当前代码 所在 的分支**

![image-20220915202248925](C:\Users\lenovo\AppData\Roaming\Typora\typora-user-images\image-20220915202248925.png)



**右击需要切换的版本，可以看到 有 checkout revision ，表示切换到选择了的版本**

**切换成功后，会发现 我们的代码  发生改变，变成切换后版本内的代码**

![image-20220915202714815](C:\Users\lenovo\AppData\Roaming\Typora\typora-user-images\image-20220915202714815.png)



## 23.Git-idea集成Git-创建分支&切换分支



点击右下角 版本号位置会出现图中所示；

可以看到 目前只有 一个分支

+号  则可以创建 分支

![image-20220915204109942](C:\Users\lenovo\AppData\Roaming\Typora\typora-user-images\image-20220915204109942.png)

输入分支名，勾选  创建完成后将自动  切换到 新创建的 分支下，第二张图 表示 切换成功

**![image-20220915204535172](C:\Users\lenovo\AppData\Roaming\Typora\typora-user-images\image-20220915204535172.png)**



![image-20220915204450268](C:\Users\lenovo\AppData\Roaming\Typora\typora-user-images\image-20220915204450268.png)

再点 右下角 ，可以看到 hot-fix 只能改名，没有 checkout

选择 master 这可以看到 有checkout ，并且 可以看到  黄色笔 head  在 hot-fix 位置，这就表示选择所在分支是hot-fix   。

当我们选择  checkout 时 ，就会 切换到指定的 版本上。

![image-20220915204910868](C:\Users\lenovo\AppData\Roaming\Typora\typora-user-images\image-20220915204910868.png)

![image-20220915205045603](C:\Users\lenovo\AppData\Roaming\Typora\typora-user-images\image-20220915205045603.png)



**我们可以在创建一个分支验证看看：**

**多次对比，我们可以知道当我们不确定所在分支时，可以看 黄色笔 head  在哪里 就可以知道 分支所在处**

![image-20220915205404099](C:\Users\lenovo\AppData\Roaming\Typora\typora-user-images\image-20220915205404099.png)

![image-20220915205454469](C:\Users\lenovo\AppData\Roaming\Typora\typora-user-images\image-20220915205454469.png)



## 24.Git-idea集成Git-合并分支（正常合并）



现在黄色 笔 在 master 上，hot-fix 上面的 代码 已被修改并提交过了，

现在需要将  hot-fix  合并到 master 中来

![image-20220915210458091](C:\Users\lenovo\AppData\Roaming\Typora\typora-user-images\image-20220915210458091.png)

依旧是点击  右下角 ，选择  需要   **被合并   的  hot-fix分支 ，**选择merge into current

**意思就是：  在 master 当前分支 上选择  hot-fix 分支 ，将它hot-fix 分支 合并到 当前 分支 master上。**

![image-20220915210732414](C:\Users\lenovo\AppData\Roaming\Typora\typora-user-images\image-20220915210732414.png)

查看代码，发现已经改变，说明合并成功。

![image-20220915211637227](C:\Users\lenovo\AppData\Roaming\Typora\typora-user-images\image-20220915211637227.png)



## 25.Git-idea集成Git-合并分支（冲突合并）



在 hot-fix分支和master上做不同的修改，产生合并冲突

![image-20220916102340296](C:\Users\lenovo\AppData\Roaming\Typora\typora-user-images\image-20220916102340296.png)



这里 漏了一个截图 ，但是理解了其实是一样的，也就是  手动点击  X 旁边的 箭头，按照自己需要的 插入顺序插进去即可

!![image-20220916103235355](C:\Users\lenovo\AppData\Roaming\Typora\typora-user-images\image-20220916103235355.png)(C:\Users\lenovo\AppData\Roaming\Typora\typora-user-images\image-20220916102532509.png)

![image-20220916102614150](C:\Users\lenovo\AppData\Roaming\Typora\typora-user-images\image-20220916102614150.png)

![image-20220916102736349](C:\Users\lenovo\AppData\Roaming\Typora\typora-user-images\image-20220916102736349.png)



## 26.Git-码云-创建远程库



类似于 GitHub，同样使用 控制面板 Git remote -v，Git remote add （取别名）（远程库地址），

Git push （别名）（分支名）；Git pull （别名）（分支名），Git clone （远程地址）；命令操作等



## 27.idea 集成码云

先要安装  Gitee  码云 插件，安装完成后 如图所示，添加  Gitee 个人账号

![image-20220916155151581](C:\Users\lenovo\AppData\Roaming\Typora\typora-user-images\image-20220916155151581.png)

点击 Git  可以发现有 相关 操作选择

![image-20220916155414474](C:\Users\lenovo\AppData\Roaming\Typora\typora-user-images\image-20220916155414474.png)

可以选择 define remote 自定义 远程库的 链接，name相当于给 远程库取 别名 url  则是 输入 你具体要push 到 Gitee 的哪个远程库上。

![image-20220916155954298](C:\Users\lenovo\AppData\Roaming\Typora\typora-user-images\image-20220916155954298.png)



![image-20220916160144918](C:\Users\lenovo\AppData\Roaming\Typora\typora-user-images\image-20220916160144918.png)

将 hot-fix  push 到 

![image-20220916155652081](C:\Users\lenovo\AppData\Roaming\Typora\typora-user-images\image-20220916155652081.png)





